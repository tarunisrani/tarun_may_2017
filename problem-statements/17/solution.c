//
//  CODEHAUS_QUES_17.c
//  
//
//  Created by Tarun Israni on 5/13/17.
//
//

#include <stdio.h>
#include <string.h>

int main(){
    int t, i, j, k, l, m, n;
    char collect[16];
    scanf("%d", &t);
    while(t--){
        scanf("%d", &n);
        char list[n][11];
        for(i=0;i<n;i++){
            scanf("%s", list[i]);
        }
        scanf("%s", collect);
        
        char temp[11];
        for(i=0;i<n;i++){
            for(j=i+1;j<n;j++){
                if(strcmp(list[i],list[j])>0){
                    strcpy(temp,list[i]);
                    strcpy(list[i],list[j]);
                    strcpy(list[j],temp);
                }
            }
        }
        for(i=0;i<n;i++){
            char destination[16];
            char str[11];
            strcpy(destination, collect);
            int match = 1;
            for(j=0;list[i][j]!='\0';j++){
                int found = 0;
                for(k=0;destination[k]!='\0';k++){
                    if(destination[k]!=0 && list[i][j] == destination[k]){
                        destination[k] = '.';
                        found = 1;
                        break;
                    }
                }
                if(found == 0){
                    match = 0;
                    break;
                }
            }
            if(match == 1){
                printf("%s ", list[i]);
            }
        }
        printf("\n");
    }
    
    return 0;
    
}