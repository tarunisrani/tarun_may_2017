import java.util.Scanner;
import java.util.*;


public class solution {

	public static void main(String args[]){
		Scanner in = new Scanner(System.in);
		String frnd_1, frnd_2;
		int t, n, i, j, k;
		t = in.nextInt();
		for(k=0;k<t;k++){
			n = in.nextInt();
			ArrayList<String> names = new ArrayList<String>();
			int arr[][] = new int[n][n];
			for(i=0;i<n;i++){
				names.add(in.next());
			}
			for(i=0;i<n;i++){
				for(j=0;j<n;j++){
					arr[i][j] = in.nextInt();
				}
			}
			frnd_1 = in.next();
			frnd_2 = in.next();

			int index_1 = names.indexOf(frnd_1);
			int index_2 = names.indexOf(frnd_2);
			int cost = search(index_1, index_2, arr, 0);
			System.out.println(cost);
		}
	}


	public static int search(int source, int dest, int arr[][], int value){
		
		int a = 0;
		int b = 0;
		
		if(arr[source][dest] == 1){
			return value+1;
		}
		int cost = Integer.MAX_VALUE;
		for(int i=0;i<arr[source].length;i++){
			if(i==source)
				continue;
			
			if(arr[source][i] == 1){
				arr[i][source] = 0;
				a = search(i,dest,arr, value+1);
			}
			if(a < cost){
				cost = a;
			}
		}
		return value+cost;

	}

}
