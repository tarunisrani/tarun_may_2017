## Instructions
Each of you will be assigned three problem statements. Your task is to validate the problems, solve and generate test cases for them. You are to also tag each question with difficulty level (easy, medium, hard), and the concept tested.

To get access to these problem statements, go to vsity.in/ccpmar18 and fork the repository containing the problem statements that you have been assigned.

Here's how each problem should be tagged (Taking the example of Problem Statement 1)

**Problem Number: 1**
**Problem Type: Easy**
**Problem Concept: Number Theory**

All numbers raised to the power of 3 and ending in 888 are known as Three-eightening numbers. 
Write a program that decides whether a given number is a Three-Eightening number or not.

**Input Format:**
The first line contains the number of testcases T.
Then T lines follow consisting of integers N

**Output Format:**
For each test-case print Y, if the number is Three-Eightening, or N if the number is not.
  
**Constraints:**
1 <= T <= 1000000
1 <= Y <= 1000000

**Sample Input:**
```
1
192
```
**Sample Output:**
```
Y
```
